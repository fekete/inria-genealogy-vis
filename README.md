# Inria Genealogy Visualization Project

Set of tools and scripts to visualize the history of Inria teams.

## Scientific Organization of Inria

The atomic organization of Inria research is the *Project-Team*, the basic research group.
Universities are organized around laboratories, that are typically larger, change their director every once in a while, and have no defined lifetime. An Inria *Project-Team* is typically smaller than a laboratory, is led by one person, and has a bounded lifetime.
A *project-team* is created within an *Inria Research Center* (there are 8 of them localized around France mostly), through a process that takes about one year nowadays but used to be longer. Along this process, a *Team* is initially created after an internal review by the Center, and becomes a *Project-Team* after an evaluation phase carried-out with internal and external reviewers.
Every year all the *Teams* write a report, available here: https://raweb.inria.fr/

A *Project-Team* is made of researchers from Inria and sometimes from affiliated organizations (it is then called joint team with University X or Organization Y) but not always (it is then called a proper [Inria] team). Sizes vary from 2-50+ with a median around 10-15. A *Project-Team* has permanent researchers from Inria and joint organizations, PhD students, an assistant shared with 3-5 other Teams, sometimes post-doctoral fellows, interns, and engineers. A few Teams have no Inria researcher but this is considered an issue that should be fixed through hiring or an internal move between teams.

Inria project-teams are created for a duration of four years that can be extended to 12 years. They undergo an evaluation every four years, and that evaluation decides if the project-team can continue as it is, if it should reorganize, or end.  After 12 years, the project-team is required to change and undergo another *Team* and *Project-Team* creation process. In the past, the *Project-Team* had to change name but this is not always the case any more since 2019.

The Inria database called BASTRI keeps track of the genealogy of *Project-Teams*. It distinguishes several possible events over the start, lifetime, and end of project teams: the creation of a team, its promotion to project-team, the creation of a "spin-off", and then at the end of the team, it can either splits into (usually) two teams, or become another team, or it can stop its existence. In the latter case, the members are dispatched to other teams or they retire. It some rare case, Inria researchers are not part of a team but directly under an Inria Research Center.

Finally, researchers can also take an administrative role. They sometimes stay within their former team or become part of the administrative organization of Inria.


## Visualization

Take a look at `inria-teams.svg` for a start. Modern web browsers can visualize it.

## Preparation

All the important files are already in the repository. To regenerate them, the process is the following:

To generate the main file, `inria-teams.json`, the contents of the Inria Bastri database should be scraped.

Connect to the Inria VPN first, and use the following command:
```
wget -r 'https://bastri.inria.fr/FichesEquipes/structurerecherche/show/'
```

Then get publication data from HAL, run:
```
sh HAL-downloads.sh
```

it produces two files: `HAL-teams.json`, the list of Inria teams over time as seen by HAL, and `HAL-articles.json`, the list of Inria-authored articles as seen by HAL.

## Processing

To generate all the files, use the `make` command:

```
make
```

If you are inside Inria's firewall or are connected through its VPN, you can scrape the Team database "Bastri" using the command:

```
make scrape
```

It will create a local directory bastri.inria.fr.
To rebuild the `inria-teams.json` data file, type:

```
rm inria-teams.json
make
```
