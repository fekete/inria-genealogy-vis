import re
from collections import defaultdict
from bs4 import BeautifulSoup

TEAMS = {}
RNSR = defaultdict(list)
NODE_YEAR = defaultdict(list)
LINK_YEAR = defaultdict(list)
ALIAS = {}


def team_equals(team1, team2):
    index = team1.find("(")
    return team2.find("(") == index and team1[:index] == team2[:index]


def strip(text):
    return re.sub(r"\s+", " ", text).strip()


REL_PATTERN = re.compile(
    r"([a-zA-Z0-9\-\. ]+ \([^\)]+\)) ([^A-Z]*) ([a-zA-Z0-9\-\. ]+ \([^\)]+\))"
)

REL_SUCCESSION = "becomes"
REL_ALIAS = "sameas"
REL_SPLIT = "spins-off"
REL_BRANCH = "branches"
REL_CONTINUE = "continues"


def add_team(team):
    TEAMS[team["id"]] = team
    team["index"] = len(TEAMS)


def parse_rel(genealogy_detail):
    if not genealogy_detail:
        return None
    match = REL_PATTERN.fullmatch(genealogy_detail)
    if not match:
        print("Detail does not match: ", genealogy_detail)
        return "", genealogy_detail, ""
    left, rel, right = match.groups()
    rel = rel.strip()
    if rel == "succession en":
        rel = REL_SUCCESSION
    elif rel == "\u00e9clatement en":
        rel = REL_SPLIT
    elif rel in ("essaimage en", "en"):
        rel = REL_BRANCH
    else:
        print("Unknown relation: ", rel, "in ", genealogy_detail)
    return left, rel, right


def process(filename):
    with open(filename) as fp:
        epi = BeautifulSoup(fp, "html.parser")
        node = {"filename": filename}
        node["id"] = filename[filename.rfind("/") + 1 :]
        encadre = epi.find("div", class_="encadre")
        name = list(encadre.find("h1").stripped_strings)
        node["name"] = name[0]
        node["fullname"] = " ".join(name)
        node["longname"] = encadre.find("p", class_="libelleLong").get_text()
        genealogie = epi.find("p", class_="genealogie")
        node["genealogy"] = strip(genealogie.get_text())
        titles = genealogie.find_all("a", title=True)
        node["genealogy_details"] = [
            parse_rel(a.attrs["title"]) for a in titles
        ]
        node["head"] = strip(
            epi.find("strong", string="Responsable :").next_sibling
        )
        node["domain"] = strip(
            epi.find("strong", string=re.compile("Domaine")).next_sibling
        )
        node["theme"] = strip(
            epi.find("strong", string="Thème :").next_sibling
        )
        rnsr = strip(epi.find("strong", string="Numéro RNSR : ").next_sibling)
        if rnsr:
            node["RNSR"] = rnsr
            docs = RNSR.get(rnsr, [])
            for doc in docs:
                if node["name"].upper() != doc["acronym_s"].upper():
                    print(
                        f"HAL acronym for RNSR={rnsr} is '{doc['acronym_s']}' instead of '{node['name']}"
                    )
                if "HALid" in node:
                    node["HALid"].append(doc["docid"])
                else:
                    node["HALid"] = [doc["docid"]]
        else:
            print(f"Missing HALid for '{node['name']}'")
        period = strip(epi.find("strong", string="Période : ").next_sibling)
        start = period[: period.find(" ")]
        end = period[period.rfind(" ") + 1 :]
        node["period"] = [start, end]
        add_team(node)


def add_hal(docs):
    for doc in docs:
        rnsr = doc["rnsr_s"][0]
        RNSR[rnsr].append(doc)


if __name__ == "__main__":
    import sys
    import json

    with open("HAL-teams.json") as inp:
        add_hal(json.load(inp))
    for arg in sys.argv[1:]:
        process(arg)
    with open("inria-teams.json", "w") as fp:
        json.dump(TEAMS, fp, indent=2, sort_keys=True)
