from collections import defaultdict
import json

TEAMS = {}
TEAMS_FULL = {}
TEAMS_NAMED = defaultdict(list)
NODE_YEAR = defaultdict(set)
LINK_YEAR = defaultdict(set)
CLUSTERS = defaultdict(set)
LINK_CLUSTER = defaultdict(set)

REL_SUCCESSION = "becomes"
REL_ALIAS = "sameas"
REL_SPLIT = "spins-off"
REL_BRANCH = "branches"
REL_CONTINUE = "continues"


def team_equals(team1, team2):
    index = team1.find("(")
    return team2.find("(") == index and team1[:index] == team2[:index]


def period_year(period):
    return int(period.split("/")[2])


def link2json(link):
    return {"source": link[0], "type": link[1], "target": link[2]}


def link_team(team):
    name = team["name"]
    fullname = team["fullname"]
    start_year = period_year(team["period"][0])
    end_year = period_year(team["period"][1])
    for year in range(start_year, end_year + 1):
        yname = name + " " + str(year)
        NODE_YEAR[year].add(yname)
        CLUSTERS[name].add(yname)
    for year in range(start_year, end_year):
        LINK_CLUSTER[name].add(
            (name + " " + str(year), REL_CONTINUE, name + " " + str(year + 1))
        )
    for detail in team["genealogy_details"]:
        if detail[2] == fullname:
            other = TEAMS_FULL[detail[0]]
            other_year = min(start_year, period_year(other["period"][1]))
            rel = detail[1]
            link = (
                other["name"] + " " + str(other_year),
                rel,
                name + " " + str(start_year),
            )
            if link[0] == link[2]:
                continue
            if other["name"] == name and rel == REL_SUCCESSION:
                LINK_CLUSTER[name].add(link)
            else:
                LINK_YEAR[end_year].add(link)


def link_teams():
    for team in TEAMS.values():
        link_team(team)


def write_graph(filename):
    nodes = []
    layers = {}
    links = []
    clusters = {}
    years = sorted(NODE_YEAR.keys())
    for year in years:
        layers[year] = list(NODE_YEAR[year])
        nodes += layers[year]
    cluster_cnt = 0
    for name, clinks in LINK_CLUSTER.items():
        cluster = {"index": cluster_cnt, "name": name}
        # 'links': [{'source': link[0], 'target': link[2]} for link in clinks]}
        cluster["ids"] = [team["id"] for team in TEAMS_NAMED[name]]
        cluster["fullnames"] = [team["fullname"] for team in TEAMS_NAMED[name]]
        cluster["domains"] = list({team["domain"] for team in TEAMS_NAMED[name]})
        cluster["themes"] = list({team["theme"] for team in TEAMS_NAMED[name]})
        cluster_cnt += 1
        cnodes = set()
        for link in clinks:
            cnodes.add(link[0])
            cnodes.add(link[2])
        cluster["nodes"] = sorted(cnodes)
        clusters[name] = cluster

    for otherlinks in LINK_YEAR.values():
        duplicates = set()
        for link in otherlinks:
            source = link[0]
            target = link[2]
            if link in duplicates or source == target:
                continue
            duplicates.add(link)
            links.append(link2json(link))
    with open(filename, "w") as out:
        graph = {
            "nodes": nodes,
            "links": links,
            "layers": layers,
            "clusters": clusters,
        }
        json.dump(graph, out, indent=2)


def process_teams():
    for team in TEAMS.values():
        fullname = team["fullname"]
        TEAMS_FULL[fullname] = team
        TEAMS_NAMED[team["name"]].append(team)


if __name__ == "__main__":
    with open("inria-teams.json") as inp:
        TEAMS = json.load(inp)
    process_teams()
    # post_process()
    link_teams()
    write_graph("inria-teams.graph")
