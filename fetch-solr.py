import requests


def fetch_solr(solr_url):
    # Starting values
    start = 0
    rows = 1000  # static, but easier to manipulate if it's a variable
    base_url = "{0}&rows={1}&start={2}"

    url = base_url.format(solr_url, rows, start)
    req = requests.get(url=url)
    out = req.json()

    total_found = out.get("response", {}).get("numFound", 0)

    # Up the start with 1000, so we fetch the next 1000
    start += rows

    results = out.get("response", {}).get("docs", [])
    all_results = results

    # Results will be an empty list if no more results are found
    while start < total_found:
        # Rebuild url base on current start.
        print(start, total_found)
        url = base_url.format(solr_url, rows, start)
        req = requests.get(url=url)
        out = req.json()
        results = out.get("response", {}).get("docs", [])
        start += rows
        all_results += results
    return all_results


if __name__ == "__main__":
    import sys
    import json

    if len(sys.argv) != 3:
        print("syntax:", sys.argv[0], " solr-url outfile")
        sys.exit(1)
    HAL = fetch_solr(sys.argv[1])
    with open(sys.argv[2], "w") as fp:
        json.dump(HAL, fp, indent=2)
