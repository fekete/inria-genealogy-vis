from collections import defaultdict, Counter
import json


TEAMS = {}


def process_arts(arts):
    collabs = defaultdict(Counter)
    for article in arts:
        if (
            "rteamStructRnsrIdExt_s" not in article
            or len(article["rteamStructRnsrIdExt_s"]) < 1
            or "publicationDateY_i" not in article
        ):
            continue
        year = article["publicationDateY_i"]
        if year < 1970:
            print(f"Publication date invalid: {year} for {article['halId_s']}")
            continue
        rnsrs = set(article["rteamStructRnsrIdExt_s"])
        fullnames = [
            TEAMS[rnsr]["fullname"] for rnsr in rnsrs if rnsr in TEAMS
        ]
        if len(fullnames) < 1:
            continue
        teams = ":".join(sorted(fullnames))
        collabs_year = collabs[year]
        collabs_year.update([teams])
    return collabs


def index_teams(teams):
    for teamid, team in teams.items():
        if "RNSR" not in team:
            print(f"No RNSR for team {team['fullname']}")
            continue
        rnsr = team["RNSR"]
        TEAMS[rnsr] = team


if __name__ == "__main__":
    with open("inria-teams.json") as inp:
        index_teams(json.load(inp))
    with open("HAL-articles.json") as inp:
        ARTS = json.load(inp)
    COLLAB = process_arts(ARTS)
    with open("inria-collab.json", "w") as out:
        json.dump(COLLAB, out, indent=2, sort_keys=True)
