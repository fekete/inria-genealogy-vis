from collections import defaultdict

TEAMS = {}
TEAMS_FULL = {}
TEAMS_HALID = {}
NODE_YEAR = defaultdict(set)
LINK_YEAR = defaultdict(set)
CLUSTERS = defaultdict(set)
LINK_CLUSTER = defaultdict(set)
AUTHOR = defaultdict(list)

REL_SUCCESSION = "becomes"
REL_ALIAS = "sameas"
REL_SPLIT = "spins-off"
REL_BRANCH = "branches"
REL_CONTINUE = "continues"
REL_PUB = "publication"

DO_CLUSTER = False


def team_equals(team1, team2):
    index = team1.find("(")
    return team2.find("(") == index and team1[:index] == team2[:index]


def period_year(period):
    return int(period.split("/")[2])


def link_team(team):
    start, end = team["period"]
    name = team["name"]
    fullname = team["fullname"]
    start_year = period_year(start)
    end_year = period_year(end)
    for year in range(start_year, end_year + 1):
        yname = name + " " + str(year)
        NODE_YEAR[year].add(yname)
        CLUSTERS[name].add(yname)
    for year in range(start_year, end_year):
        LINK_CLUSTER[name].add(
            (name + " " + str(year), REL_CONTINUE, name + " " + str(year + 1))
        )

    for detail in team["genealogy_details"]:
        if detail[2] == fullname:
            other = TEAMS_FULL[detail[0]]
            other_year = min(start_year, period_year(other["period"][1]))
            rel = detail[1]
            link = (
                other["name"] + " " + str(other_year),
                rel,
                name + " " + str(start_year),
            )
            if link[0] == link[2]:
                continue
            if other["name"] == name and rel == REL_SUCCESSION:
                LINK_CLUSTER[name].add(link)
            else:
                LINK_YEAR[end_year].add(link)


def link_teams():
    for team in TEAMS.values():
        link_team(team)


def write_clusters(out):
    cluster_cnt = 0
    for cluster, links in LINK_CLUSTER.items():
        if DO_CLUSTER:
            print(f"subgraph cluster_{cluster_cnt} {{", file=out)
            cluster_cnt += 1
            print(f' label = "{cluster}"; color=black;', file=out)
        for link in sorted(links):
            source = link[0]
            target = link[2]
            rel = link[1]
            style = rel_style(rel)
            print(f' "{source}"->"{target}"{style};', file=out)
        if DO_CLUSTER:
            print("}", file=out)


def write_dot(filename):
    with open(filename, "w") as out:
        print("digraph inria { rankdir=LR; newrank=true;", file=out)
        years = sorted(NODE_YEAR.keys())
        print("{ node [shape=plaintext, fontsize=12];", file=out)
        print(f'"{years[0]}"', end="", file=out)
        for year in years[1:]:
            print(f'->"{year}"', end="", file=out)
        print(";}", file=out)
        print("node [shape=box, fontsize=12];", file=out)
        for key in years:
            teams = NODE_YEAR[key]
            print(f'{{rank=same; "{str(key)}"', end="", file=out)
            for team in teams:
                print(f'; "{team}"', end="", file=out)
            print("}", file=out)
        write_clusters(out)
        for links in LINK_YEAR.values():
            duplicates = set()
            for link in links:
                source = link[0]
                target = link[2]
                rel = link[1]
                if link in duplicates or source == target:
                    continue
                duplicates.add(link)
                style = rel_style(rel)
                if rel == REL_PUB:
                    print(f'"{target}"{style};', file=out)
                print(f'"{source}" -> "{target}"{style};', file=out)
        print("}", file=out)


def rel_style(rel):
    if rel == REL_SUCCESSION:
        return '[style="bold"]'
    if rel == REL_ALIAS:
        return '[style="bold", color="red"]'
    if rel in [REL_SPLIT, REL_BRANCH]:
        return '[style="dotted"]'
    if rel == REL_PUB:
        return '[style="invis"]'
    return ""


def process_teams():
    for team in TEAMS.values():
        fullname = team["fullname"]
        TEAMS_FULL[fullname] = team
        if "HALid" in team:
            for halid in team["HALid"]:
                TEAMS_HALID[halid] = team
        # else:
        #     print("Missing HALid for ",
        #           fullname,
        #           period_year(team['period'][0]),
        #           period_year(team['period'][1]))
        # for detail in team['genealogy_details']:
        #     if detail[2] == fullname and detail[1] == REL_SUCCESSION \
        #        and team_equals(fullname, detail[0]):
        #         TO_MERGE.add((detail[0], fullname))


def same(items):
    items = list(items)
    first = items[0]
    for item in items[1:]:
        if item != first:
            return False
    return True


def process_pubs(pubs):
    for article in pubs:
        # authors = article['authFullName_s']
        structs = article["authStructId_i"]
        docid = article["docid"]
        struct_team = {}
        for struct in structs:
            team = TEAMS_HALID.get(struct)
            if team:
                # print("Found ", team['fullname'])
                struct_team[struct] = team["fullname"]
        teams = set(struct_team.values())
        year = article["publicationDateY_i"]
        if len(teams) > 1:
            for fullname in teams:
                team = TEAMS_FULL[fullname]
                print(fullname, end=" ")
                link_year = year
                # link_year = max(year, period_year(team['period'][0]))
                # link_year = min(year, period_year(team['period'][1]))
                # if link_year != year:
                #     print(f" year fixed {year}->{link_year} ")
                LINK_YEAR[year].add(
                    (team["name"] + " " + str(link_year), REL_PUB, docid)
                )
            print()


if __name__ == "__main__":
    import sys
    import json

    if len(sys.argv) == 2 and sys.argv[1] == "-h":
        print("Syntax:", sys.argv[0], " inria-teams.json [HAL-articles.json]")
        sys.exit(0)
    INFILE = "inria-teams.json"
    if len(sys.argv) > 1:
        INFILE = sys.argv[1]
    with open(INFILE) as inp:
        TEAMS = json.load(inp)
    process_teams()
    if len(sys.argv) > 2:
        with open(sys.argv[2]) as inp:
            process_pubs(json.load(inp))
    # post_process()
    link_teams()
    write_dot("inria-teams.dot")
