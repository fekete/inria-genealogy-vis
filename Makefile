.suffixes: .json .py .sh .graph
.PHONY: black clean scrape

all: inria-collab.json HAL-articles.json HAL-teams.json inria-teams.json inria-teams.graph inria-teams.dot inria-teams.svg

inria-collab.json: HAL-articles.json inria-teams.json art2collab.py
	python art2collab.py

HAL-articles.json HAL-teams.json: HAL-downloads.sh
	sh HAL-downloads.sh

inria-teams.json: HAL-teams.json bastri2json.py # bastri.inria.fr/FichesEquipes/structurerecherche/list/0
	python bastri2json.py bastri.inria.fr/FichesEquipes/structurerecherche/show/*

inria-teams.graph: json2json.py inria-teams.json
	python json2json.py

inria-teams.dot: inria-teams.json
	python json2dot

inria-teams.svg: inria-teams.dot
	dot -Tsvg inria-teams.dot -o inria-teams.svg

clean:
	rm -f inria-collab.json HAL-articles.json HAL-teams.json inria-teams.json inria-teams.graph

black:
	black -l 79 *.py

scrape:
	wget -r -N -I '/FichesEquipes/structurerecherche/show' 'https://bastri.inria.fr/FichesEquipes/structurerecherche/list/0'
